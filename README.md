# Wikipedia transposition bot

## Twitter
This code grabs a random title from Wikipedia, does some _highly advanced_ tests, and transposes the title.
It then tweets: https://twitter.com/wiki_transposed

## Demo page
[Live demo here.](http://wikibot.wengerd.org)

## Examples

```
Fading West (EP)? More like Wading Fest (EP).
Kate Richardson? More like Rate Kichardson.
Nick Kuipers (footballer, born 1992)? More like Kick Nuipers (footballer, born 1992).
```
