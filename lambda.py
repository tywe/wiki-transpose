import tweepy
from wikibot import get_article, swap_title, create_link
import os

def create_tweet():
    article = get_article()
    swapped_title, original_title = swap_title(article)
    link = create_link(original_title)
    tweet = f"{original_title}? More like {swapped_title}. {link}"
    return tweet


def get_api():
    auth = tweepy.OAuthHandler(os.environ['consumer_key'], os.environ['consumer_secret'])
    auth.set_access_token(os.environ['access_token'], os.environ['access_token_secret'])
    return tweepy.API(auth)


def send_tweet(tweet):
    api = get_api()
    status = api.update_status(status=tweet)


def handler(event, context):
    tweet = create_tweet()
    print(tweet)
    send_tweet(tweet)


if __name__ == "__main__":
    handler(None, None)