import wikipedia

def get_two_main_words(title):
    title = title.split(" ")
    determiners = ["the", "a", "my"]
    prepositions = ["of", "in", "for"]
    if len(title) == 2 and title[0].lower() not in determiners and title[0].lower() not in prepositions:
        return title
    if len(title) == 3 and title[0].lower() in determiners:
        # The Italian Job
        return title[1], title[2]
    if len(title) == 3 and title[1].lower() in prepositions:
        # Blades of Steel
        return title[0], title[2]
    if len(title) == 4 and title[0].lower() in determiners and title[2].lower() in prepositions:
        # A Game of Thrones
        return title[1], title[3]
    if len(title) == 4 and title[1].lower() in prepositions and title[2].lower in determiners:
        # Gorillas in the Mist
        return title[0], title[3]
    return False


def separate_parens(whole_title):
    if "(" in whole_title:
        title = whole_title[0:whole_title.index("(")-1]
        parens = whole_title[whole_title.index("("):]
        return title, parens
    else:
        return whole_title, False


def validate_first_chars(title):
    vowels = ["a", "e", "i", "o", "u"]
    firstword = title[0]
    secondword = title[1]
    if (
        firstword[0].isalpha() and secondword[0].isalpha() and
        firstword[0].lower() != secondword[0].lower() and
        (
            (firstword[0].lower() in vowels and secondword[0].lower() in vowels) or
            (firstword[0].lower() not in vowels and secondword[0].lower() not in vowels)
        )
    ):
        return True
    else:
        return False


def validate_references(title):
    min_reference_count = 5
    try:
        page = wikipedia.WikipediaPage(title=title)
        references = page.references
        if len(references) >= min_reference_count:
            return True
    except wikipedia.exceptions.DisambiguationError:
        return False
    except KeyError:
        return False
    return False


def get_article():
    two_main_words = False
    works_phonetically = False
    has_enough_references = False
    is_good_enough_article = False

    while not is_good_enough_article:
        original_title = wikipedia.random()
        title, parens = separate_parens(original_title)
        two_main_words = get_two_main_words(title)
        if two_main_words:
            works_phonetically = validate_first_chars(two_main_words)
            if works_phonetically:
                has_enough_references = validate_references(original_title)
                if has_enough_references:
                    is_good_enough_article = True
    return {
        "original_title": original_title,
        "words_to_swap": two_main_words,
    }


def swap_title(article):
    words_to_swap = article["words_to_swap"]
    swapped_title = article["original_title"].split(" ")
    first_word_index = swapped_title.index(words_to_swap[0])
    second_word_index = swapped_title.index(words_to_swap[1])

    first_swapped_word = list(swapped_title[first_word_index])
    second_swapped_word = list(swapped_title[second_word_index])

    # make sure KISS discography becomes DISS kiscography, etc
    if words_to_swap[0][0].isupper():
        first_swapped_word[0] = words_to_swap[1][0].upper()
    else:
        first_swapped_word[0] = words_to_swap[1][0].lower()

    if words_to_swap[1][0].isupper():
        second_swapped_word[0] = words_to_swap[0][0].upper()
    else:
        second_swapped_word[0] = words_to_swap[0][0].lower()

    first_swapped_word = "".join(first_swapped_word)
    second_swapped_word = "".join(second_swapped_word)

    swapped_title[first_word_index] = first_swapped_word
    swapped_title[second_word_index] = second_swapped_word

    return " ".join(swapped_title), article["original_title"]


def create_link(title):
    link = "https://en.wikipedia.org/wiki/{title}".format(
        title=title.replace(" ", "_")
    )
    return link
