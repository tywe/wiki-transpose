import web
from wikibot import get_article, swap_title, create_link

urls = (
  '/', 'Index'
)

app = web.application(urls, globals())

render = web.template.render('templates/')

class Index(object):
    def GET(self):

        # try:
        article = get_article()
        swapped_title, original_title = swap_title(article)
        original_title = original_title
        swapped_title = swapped_title
        link = create_link(original_title)
        return render.index(
            swapped_title = swapped_title,
            original_title = original_title,
            link = link
        )
        # except Exception as e:
        #     return render.error()

if __name__ == "__main__":
    app.run()
